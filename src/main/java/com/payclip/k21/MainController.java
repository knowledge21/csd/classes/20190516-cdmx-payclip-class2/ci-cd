package com.payclip.k21;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.websocket.server.PathParam;

@RestController
public class MainController {

    @RequestMapping("/{amount}")
    public String index(@PathVariable Double amount) {
        return String.valueOf(CommissionCalculator.calculate(amount));
    }

}