FROM java:8-jdk

COPY ./build/libs/calculator-0.1.0.jar .

EXPOSE 8080

CMD java -jar calculator-0.1.0.jar